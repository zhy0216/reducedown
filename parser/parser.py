﻿# -*- coding: utf-8 -*-

import re


_header1 = re.compile(r'^#(?!#)(?P<header>.+)', re.M)
_header2 = re.compile(r'^##(?!#)(?P<header>.+)', re.M)

_strong = re.compile(r'\*(?P<strong>.+)\*', re.M)

_auto_link = re.compile(r'(?P<link>(http|https)://.+[\b|\s]?)', re.M)



a='''
something here:
1. test1
2. nice2
3.  whatever :
    1. whats
    2. is
    3. this
'''
_order_lists = re.compile(r'(?<=:\n)(\s{0,12}\d\.)+',re.M)
_list_items = re.compile(r'[\d\s]+\. (?P<item>.*)',re.M)

def parse_head(content):
    if _header1.match(content)!=None:
        return re.sub(_header1,r'<h1>\g<header></h1>',content)
    if _header2.match(content)!=None:
        return re.sub(_header2,r'<h2>\g<header></h2>',content)
    return content

def parse_bold(content):
    return re.sub(_strong,r'<strong>\g<strong></strong>',content)


def auto_link(content):
    return re.sub(_auto_link,r'<a href="\g<link>">\g<link></a>',content)

def parse_list_item(item):
    return re.sub(_list_items,r'<li>\g<item></li>',item)

def _parse_ordered_list(content, oldcontent =""):
    result = _order_lists.search(content)
    # print "the content is", content
    # print "the result is", result
    if result is None:
        return content, content
    else:
        start_index = result.start(1)
        temp_content = content[start_index:]
        result, oldcontent = _parse_ordered_list(temp_content, oldcontent)

    # print "back: ", result
    # print "temp_content: ", temp_content
    result_content = "<ol>"
    if result != oldcontent:
        temp_content = temp_content.replace(oldcontent, result)
    result_content += parse_list_item(temp_content)
    result_content += "</ol>"
    return result_content, oldcontent

def parse_ordered_list(content):
    return _parse_ordered_list(content)[0]




















