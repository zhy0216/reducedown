﻿# -*- coding: utf-8 -*-

from nose.tools import istest, nottest

from reducedown.parser.parser import parse_head,parse_bold,auto_link


def eq(result, expect_result):
    print "the result: ", result
    print "expect result:", expect_result
    assert  result == expect_result

def test_header1():
    content = "#good good study"
    expect_result = '<h1>%s</h1>'%content[1:]
    result = parse_head(content)
    eq(result, expect_result)


def test_header2():
    content = "##good good study"
    expect_result = '<h2>%s</h2>'%content[2:]
    result = parse_head(content)
    eq(result, expect_result)

def test_strong():
    content = "something *become strong* expection"
    expect_result = 'something <strong>%s</strong> expection'%"become strong"
    result = parse_bold(content)
    eq(result, expect_result)

def test_autolink():
    link = "http://google.com"
    content = "good good study %s"%link
    result = auto_link(content)
    expect_result ='good good study <a href="%s">%s</a>'%(link,link)
    eq(result, expect_result)













